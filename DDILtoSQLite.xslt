<?xml version="1.0" encoding="utf-8"?>
<!--
	Copyright © Neumont University. All rights reserved.

	This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.
	Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:
	1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
	2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
	3. This notice may not be removed or altered from any source distribution.
-->
<!-- Contributors: Corey Kaylor, Kevin M. Owen, Artyom Shalkhakov -->
<!-- based on DDILtoPostgreSQL.xslt
     Known issues:
     - SQLite performs case-insensitive identifier lookup and "Quoting" identifiers does not work
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:dsf="urn:schemas-orm-net:DIL:DILSupportFunctions"
	xmlns:dil="http://schemas.orm.net/DIL/DIL"
	xmlns:ddt="http://schemas.orm.net/DIL/DILDT"
	xmlns:dep="http://schemas.orm.net/DIL/DILEP"
	xmlns:dms="http://schemas.orm.net/DIL/DILMS"
	xmlns:dml="http://schemas.orm.net/DIL/DMIL"
	xmlns:ddl="http://schemas.orm.net/DIL/DDIL"
	extension-element-prefixes="exsl dsf"
	exclude-result-prefixes="dil ddt dep dms dml ddl">

	<xsl:import href="DDILtoSQLStandard.xslt"/>
	<xsl:import href="DomainInliner.xslt"/>
	<xsl:import href="TinyIntRemover.xslt"/>

	<xsl:output method="text" encoding="utf-8" indent="no" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:param name="DefaultMaximumStringLength" select="''"/>
	<xsl:param name="DefaultMaximumExactNumericPrecisionAndScale" select="''"/>
	<xsl:param name="DefaultMaximumExactNumericPrecisionWithScale" select="1000"/>
	<xsl:param name="DefaultMaximumApproximateNumericPrecision" select="''"/>

	<xsl:template match="/">
		<xsl:variable name="tinyIntRemovedDilFragment">
			<xsl:apply-templates mode="TinyIntRemover" select="."/>
		</xsl:variable>
		<xsl:variable name="domainInlinedDilFragment">
			<xsl:apply-templates mode="DomainInliner" select="exsl:node-set($tinyIntRemovedDilFragment)"/>
		</xsl:variable>
		<xsl:apply-templates select="exsl:node-set($domainInlinedDilFragment)/child::*"/>
	</xsl:template>

	<xsl:template match="@defaultCharacterSet" mode="ForSchemaDefinition"/>

	<!-- currently SQLite does not support schemas or catalogs -->
	<xsl:template match="@catalog" mode="ForSchemaQualifiedName"/>
	<xsl:template match="@schema" mode="ForSchemaQualifiedName"/>
	<xsl:template match="ddl:schemaDefinition">
		<!-- FIXME: this is necessary because we prefer to
		     have SQLite enforce FK constraints for us, but I
		     have not found a better place to put this
		     -->
		<xsl:param name="indent"/>
		<xsl:value-of select="$NewLine"/>
		<xsl:value-of select="$indent"/>
		<xsl:text>PRAGMA foreign_keys = ON</xsl:text>
		<xsl:value-of select="$StatementDelimiter"/>
		<xsl:value-of select="$NewLine"/>
	</xsl:template>
	<xsl:template match="@defaultCharacterSet" mode="ForSchemaDefinition"/>
	<xsl:template match="dms:setSchemaStatement"/>
	<xsl:template match="ddl:generationClause"/>

	<!-- the current version of SQLite does not support
	     transactions with DDL (according to my tests anyway) -->
	<xsl:template match="dms:startTransactionStatement"/>
	<xsl:template match="dms:commitStatement"/>

	<!-- this is unsupported -->
	<xsl:template match="ddl:alterTableStatement[ddl:addTableConstraintDefinition]"/>

	<xsl:template match="ddl:tableDefinition">
		<xsl:param name="indent"/>
		<xsl:value-of select="$NewLine"/>
		<xsl:value-of select="$indent"/>
		<xsl:text>CREATE </xsl:text>
		<xsl:apply-templates select="@scope" mode="ForTableDefinition"/>
		<xsl:text>TABLE </xsl:text>
		<xsl:variable name="table-catalog" select="@catalog"/>
		<xsl:variable name="table-schema" select="@schema"/>
		<xsl:variable name="table-name" select="@name"/>
		<xsl:apply-templates select="@catalog" mode="ForSchemaQualifiedName"/>
		<xsl:apply-templates select="@schema" mode="ForSchemaQualifiedName"/>
		<xsl:apply-templates select="@name" mode="ForSchemaQualifiedName"/>
		<xsl:value-of select="$NewLine"/>
		<xsl:value-of select="$indent"/>
		<xsl:value-of select="$StatementStartBracket"/>
		<xsl:apply-templates select="ddl:columnDefinition">
			<xsl:with-param name="indent" select="concat($indent, $IndentChar)"/>
		</xsl:apply-templates>
		<xsl:choose>
			<!-- skip primary key constraint rendering if an identity column is present -->
			<xsl:when test="ddl:columnDefinition[ddl:identityColumnSpecification]">
				<xsl:apply-templates select="ddl:tableConstraintDefinition[ddl:uniqueConstraintDefinition/@type != 'PRIMARY KEY']">
					<xsl:with-param name="indent" select="concat($indent, $IndentChar)"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="ddl:tableConstraintDefinition">
					<xsl:with-param name="indent" select="concat($indent, $IndentChar)"/>
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>
		<!-- output foreign keys, if any -->
		<xsl:if test="../ddl:alterTableStatement[@name=$table-name and @schema=$table-schema]/ddl:addTableConstraintDefinition">
			<xsl:text>,</xsl:text>
			<!-- FIXME: how to refer to attributes properly? -->
			<xsl:apply-templates select="../ddl:alterTableStatement[@name=$table-name and @schema=$table-schema] /ddl:addTableConstraintDefinition" mode="ForForeignKey">
				<xsl:with-param name="indent" select="concat($indent, $IndentChar)"/>
			</xsl:apply-templates>
		</xsl:if>

		<xsl:value-of select="$NewLine"/>
		<xsl:value-of select="$indent"/>
		<xsl:value-of select="$StatementEndBracket"/>
		<xsl:value-of select="$StatementDelimiter"/>
		<xsl:value-of select="$NewLine"/>
	</xsl:template>

	<xsl:template match="ddl:addTableConstraintDefinition" mode="ForForeignKey">
		<xsl:param name="indent"/>
		<xsl:if test="not(position()=1) or preceding-sibling::*">
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:value-of select="$NewLine"/>
		<xsl:value-of select="$indent"/>
		<xsl:apply-templates select="ddl:referentialConstraintDefinition"/>
	</xsl:template>

	<xsl:template match="ddl:columnDefinition[ddl:identityColumnSpecification]">
		<xsl:param name="indent"/>
		<xsl:if test="not(position()=1)">
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:value-of select="$NewLine"/>
		<xsl:value-of select="$indent"/>
		<xsl:apply-templates select="@name" mode="ForColumnName"/>
		<xsl:apply-templates select="ddt:boolean | ddt:characterString | ddt:binaryString | ddt:date | ddt:time | ddt:interval | ddt:domain"/>
		<xsl:apply-templates select="ddt:exactNumeric | ddt:approximateNumeric" mode="ForSQLiteIdentityColumn"/>
		<xsl:apply-templates select="ddl:defaultClause"/>
		<xsl:apply-templates select="ddl:generationClause"/>
		<xsl:apply-templates select="ddl:columnConstraintDefinition"/>
	</xsl:template>

	<xsl:template match="ddt:exactNumeric | ddt:approximateNumeric" mode="ForSQLiteIdentityColumn">
		<xsl:choose>
			<xsl:when test="@type='INTEGER' or @type='SMALLINT'">
				<xsl:text>INTEGER PRIMARY KEY AUTOINCREMENT</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message><xsl:value-of select="@type + ' is unsupported as identity column'"/></xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="ddt:characterString | ddt:binaryString" mode="ForDataTypeLengthWithMultiplier">
		<xsl:call-template name="GetTotalDataTypeLength"/>
	</xsl:template>

	<xsl:template match="ddt:booleanLiteral">
		<xsl:choose>
			<xsl:when test="@value='TRUE'">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="@value='FALSE'">
				<xsl:text>0</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>NULL</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="@type[.='CHARACTER LARGE OBJECT']" mode="ForDataType">
		<xsl:text>CHARACTER VARYING</xsl:text>
	</xsl:template>

	<xsl:template match="@type[.='BINARY' or .='BINARY VARYING' or .='BINARY LARGE OBJECT']" mode="ForDataType">
		<xsl:text>BLOB</xsl:text>
		<!-- UNDONE: Add constraints to support the original length requested. -->
	</xsl:template>
	
	<!-- UNDONE: Handle rendering ddt:binaryStringLiteral. -->

	<xsl:template match="ddl:sqlInvokedProcedure"/>

	<xsl:template match="ddl:sqlRoutineSpec"/>

	<xsl:template match="dep:sqlParameterReference"/>

	<xsl:template match="ddl:atomicBlock">

	</xsl:template>

</xsl:stylesheet>
