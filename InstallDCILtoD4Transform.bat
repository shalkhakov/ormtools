@ECHO OFF

IF "%ProgramFiles(X86)%"=="" (
	SET ResolvedProgramFiles=%ProgramFiles%
	SET ResolvedCommonProgramFiles=%CommonProgramFiles%
	SET WOWRegistryAdjust=
) ELSE (
  ::Do this somewhere the resolved parens will not cause problems.
  SET ResolvedProgramFiles=%ProgramFiles(x86)%
  SET ResolvedCommonProgramFiles=%CommonProgramFiles(x86)%
  ::If this batch file is already running under a 32 bit process, then the
  ::reg utility will choose the appropriate registry keys without our help.
  ::This also means that this file should not be called to pre-set environment
  ::variables before invoking 32-bit processes that use these variables.
  IF DEFINED PROCESSOR_ARCHITEW6432 (
    SET WOWRegistryAdjust=
  ) ELSE (
    SET WOWRegistryAdjust=\Wow6432Node
  )
)

IF NOT DEFINED DILDir SET DILDir=%ResolvedCommonProgramFiles%\Neumont\DIL
IF NOT DEFINED DILTransformsDir SET DILTransformsDir=%DILDir%\Transforms

rem IF NOT DEFINED RegistryRootBase (
SET RegistryRootBase=HKLM\SOFTWARE%WOWRegistryAdjust%\ORM Solutions\Natural ORM Architect for Visual Studio 2013\Generators
rem )

XCOPY /Y /D /V /Q "DCILtoD4.xslt" "%DILTransformsDir%"
XCOPY /Y /D /V /Q "DDILtoSQLite.xslt" "%DILTransformsDir%"

ECHO "%RegistryRootBase%"

REG ADD "%RegistryRootBase%\DCILtoD4"
REG ADD "%RegistryRootBase%\DCILtoD4" /v DisplayDescription /d "Transforms DCIL to D4"
REG ADD "%RegistryRootBase%\DCILtoD4" /v DisplayName /d "DCIL to D4"
REG ADD "%RegistryRootBase%\DCILtoD4" /v FileExtension /d ".d4"
REG ADD "%RegistryRootBase%\DCILtoD4" /v OfficialName /d "DCILtoD4"
REG ADD "%RegistryRootBase%\DCILtoD4" /v ProvidesOutputFormat /d "D4"
REG ADD "%RegistryRootBase%\DCILtoD4" /v SourceInputFormat /d "DCIL"
REG ADD "%RegistryRootBase%\DCILtoD4" /v TransformURI /t REG_SZ /d "%DILTransformsDir%\DCILtoD4.xslt"
REG ADD "%RegistryRootBase%\DCILtoD4" /v Type /d XSLT

REG ADD "%RegistryRootBase%\DDILtoSQLite"
REG ADD "%RegistryRootBase%\DDILtoSQLite" /v DisplayDescription /d "Transforms DDIL to SQLite-dialect SQL."
REG ADD "%RegistryRootBase%\DDILtoSQLite" /v DisplayName /d "DDIL to SQLite"
REG ADD "%RegistryRootBase%\DDILtoSQLite" /v FileExtension /d ".SQLite.sql"
REG ADD "%RegistryRootBase%\DDILtoSQLite" /v OfficialName /d "DDILtoSQLite"
REG ADD "%RegistryRootBase%\DDILtoSQLite" /v ProvidesOutputFormat /d "SQL_SQLite"
REG ADD "%RegistryRootBase%\DDILtoSQLite" /v SourceInputFormat /d "DDIL"
REG ADD "%RegistryRootBase%\DDILtoSQLite" /v TransformURI /t REG_SZ /d "%DILTransformsDir%\DDILtoSQLite.xslt"
REG ADD "%RegistryRootBase%\DDILtoSQLite" /v Type /d XSLT
