# ORM tools

This repository contains some open-source tools for ORM (Object-Role
Modeling).

In particular, the tools currently being distributed are as follows:

1. [orm2d4](https://bb.githack.com/shalkhakov/ormtools/raw/master/orm2d4/orm2d4.html):
   This is a very simple viewer for ORM model files produced by the
   NORMA tool (which is an add-in for Microsoft Visual Studio).  This
   viewer runs in a web browser using Saxon CE. It will probably be
   ported to a general-purpose programming language (such as C# or F#)
   in the future to improve performance once the design stabilises.  I
   am also keen on adding some basic editing capabilities to it.

2. [DCILtoD4](DCILtoD4.xslt): this is an XSLT 1.0 transformation that
   outputs [D4](https://en.wikipedia.org/wiki/Dataphor) schema
   definition statements. It is designed to work in conjunction with
   the [NORMA](https://sourceforge.net/projects/orm/) tool. After
   running the installation script (`InstallDCILtoD4Transform.bat`),
   please restart all running instances of *Visual Studio* (NOTE: at
   the moment, we only support VS2012, VS2013), after that, you can
   select a new generator called *D4* in the *ORMGeneratorSettings*
   dialog. This will create a new file with the extension `.d4` that
   you can subsequently load in *Dataphor* and have it create the
   corresponding internal database schema for you, based on the
   default device of your choosing.

3. [DDILtoSQLite](DDILtoSQLite.xslt): this is an XSLT 1.0
   transformation that outputs [SQLite](https://www.sqlite.org/)
   schema definition statements. Like DCILtoD4.xslt, this one is
   designed to work in conjunction with *NORMA*. Please use
   `InstallDCILtoD4Transform.bat` to install it. This will create a
   new file that you can subsequently use to create an *SQLite*
   database.

Released under BSD v3 license.

Author: Artyom Shalkhakov, artyom DOT shalkhakov AT gmail DOT com
