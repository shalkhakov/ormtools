<xsl:transform
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:ixsl="http://saxonica.com/ns/interactiveXSLT"
xmlns:js="http://saxonica.com/ns/globalJS"
xmlns:prop="http://saxonica.com/ns/html-property"
xmlns:style="http://saxonica.com/ns/html-style-property"
xmlns:math="http://www.w3.org/2005/xpath-functions/math"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:orm2d4="http://underivable.org/ns/orm2d4"
xmlns:orm="http://schemas.neumont.edu/ORM/2006-04/ORMCore" 
xmlns:ormRoot="http://schemas.neumont.edu/ORM/2006-04/ORMRoot"
xmlns:ormDiagram="http://schemas.neumont.edu/ORM/2006-04/ORMDiagram"
xmlns:svg="http://www.w3.org/2000/svg" 
exclude-result-prefixes="xs prop"
extension-element-prefixes="ixsl"
version="2.0"
>
<xsl:param name="debug-rects" select="false()"/>
<!-- FIXME: unary constraints are transformed to binary by NORMA, which I did not anticipate; fix this! -->
<!-- FIXME: where do I get SVG images of various graphical elements? e.g. constraints -->
<xsl:function name="orm2d4:lineseg-make">
<xsl:param name="x1"/>
<xsl:param name="y1"/>
<xsl:param name="x2"/>
<xsl:param name="y2"/>
<orm2d4:lineseg x1="{$x1}" y1="{$y1}" x2="{$x2}" y2="{$y2}"/>
</xsl:function>
<xsl:function name="orm2d4:point-make">
<xsl:param name="x"/>
<xsl:param name="y"/>
<orm2d4:point x="{$x}" y="{$y}"/>
</xsl:function>
<xsl:function name="orm2d4:vector-make">
<xsl:param name="x"/>
<xsl:param name="y"/>
<orm2d4:vector x="{$x}" y="{$y}"/>
</xsl:function>
<xsl:function name="orm2d4:point-move">
<xsl:param name="pt"/>
<xsl:param name="ofs"/>
<xsl:param name="dir"/>
<orm2d4:point
x="{$pt/@x + $ofs * $dir/@x}"
y="{$pt/@y + $ofs * $dir/@y}"/>
</xsl:function>
<xsl:function name="orm2d4:bounds-make">
<xsl:param name="x"/>
<xsl:param name="y"/>
<xsl:param name="width"/>
<xsl:param name="height"/>
<orm2d4:bounds x="{$x}" y="{$y}" width="{$width}" height="{$height}"/>
</xsl:function>

<!-- NOTE: the winding will be clockwise, starting at top-most left-most point -->
<xsl:function name="orm2d4:bounds-to-points">
<xsl:param name="bounds"/>
<orm2d4:seq>
<xsl:sequence select="orm2d4:point-make($bounds/@x, $bounds/@y)"/>
<xsl:sequence select="orm2d4:point-make($bounds/@x + $bounds/@width, $bounds/@y)"/>
<xsl:sequence select="orm2d4:point-make($bounds/@x + $bounds/@width, $bounds/@y + $bounds/@height)"/>
<xsl:sequence select="orm2d4:point-make($bounds/@x, $bounds/@y + $bounds/@height)"/>
</orm2d4:seq>
</xsl:function>

<!-- NOTE: clockwise winding -->
<xsl:function name="orm2d4:points-to-linesegs">
<xsl:param name="pseq"/>
<orm2d4:seq>
<xsl:for-each select="$pseq/orm2d4:point">
<xsl:variable
name="npt"
select="if (position() = last())
then $pseq/orm2d4:point[1]
else following-sibling::orm2d4:point[1]"/>
<xsl:sequence select="orm2d4:lineseg-make(@x, @y, $npt/@x, $npt/@y)"/>
</xsl:for-each>
</orm2d4:seq>
</xsl:function>

<xsl:function name="orm2d4:bounds-parse">
<xsl:param name="arg"/> <!-- as="xsd:string" -->
<xsl:variable name="bounds" select="translate($arg, ' ', '')"/>
<orm2d4:bounds
x="{number(substring-before($bounds, ','))}"
y="{number(substring-before(substring-after($bounds, ','), ','))}"
width="{number(substring-before(substring-after(substring-after($bounds, ','), ','), ','))}"
height="{number(substring-after(substring-after(substring-after($bounds, ','), ','), ','))}"/>
</xsl:function>

<xsl:function name="orm2d4:bounds-center">
<xsl:param name="arg"/> <!-- as="orm2d4:bounds" -->
<orm2d4:vec2
x="{$arg/@x + $arg/@width div 2.0}"
y="{$arg/@y + $arg/@height div 2.0}"/>
</xsl:function>

<xsl:function name="orm2d4:lineseg-center">
<xsl:param name="arg"/>
<xsl:sequence
select="orm2d4:point-make(($arg/@x2 + $arg/@x1) div 2, ($arg/@y2 + $arg/@y1) div 2)"/>
</xsl:function>

<!--
FIXME: sometimes we do not want to use an edge
because it is adjacent to another role box;
how do we pick an edge then?
-->
<xsl:function name="orm2d4:edge-index-for-point">
<xsl:param name="bounds"/>
<xsl:param name="point"/>

<xsl:variable name="center" select="orm2d4:bounds-center($bounds)"/>
<xsl:variable name="math-obj" select="ixsl:get(ixsl:window(), 'Math')"/>
<xsl:variable name="pi" select="ixsl:get($math-obj, 'PI')"/>
<xsl:variable 
name="theta0"
select="ixsl:call($math-obj, 'atan2',
$point/@y - $center/@y,
$center/@x - $point/@x)"/>
<xsl:variable name="theta" select="$theta0 + $pi"/>
<xsl:variable name="pi4" select="$pi div 4"/>
<!-- <xsl:value-of select="$theta"/>-->
<xsl:choose>
<!-- TODO: simplify symbolically -->
<xsl:when test="$theta &gt;= $pi4 and $theta &lt; $pi div 2 + $pi4">1<!-- top --></xsl:when>
<xsl:when test="$theta &gt;= $pi div 2 + $pi4 and $theta &lt; $pi + $pi4">4<!-- left --></xsl:when>
<xsl:when test="$theta &gt;= $pi + $pi4 and $theta &lt; 3*$pi div 2 + $pi4">3<!-- bottom --></xsl:when>
<xsl:otherwise>2<!-- right--></xsl:otherwise>
<!-- another classification, without tilting by 45 degrees
<xsl:when test="$theta &gt;= 0 and $theta &lt; $pi div 2"> top right</xsl:when>
<xsl:when test="$theta &gt;= $pi div 2 and $theta &lt; $pi"> top left</xsl:when>
<xsl:when test="$theta &gt;= $pi and $theta &lt; 3*$pi div 2"> bottom left</xsl:when>
<xsl:otherwise>bottom right</xsl:otherwise>-->
</xsl:choose>
</xsl:function>

<xsl:function name="orm2d4:point-distance-squared">
<xsl:param name="pt1"/>
<xsl:param name="pt2"/>
<xsl:variable name="dx" select="$pt1/@x - $pt2/@x"/>
<xsl:variable name="dy" select="$pt1/@y - $pt2/@y"/>
<xsl:sequence select="$dx * $dx + $dy * $dy"/>
</xsl:function>

<xsl:template name="main" match="/">
<xsl:result-document href="#main" method="append-content">
<h2>transformation results</h2>
<!-- FIXME: overflow spec? -->
<svg:svg width="758.4" height="480" viewBox="0 0 632 400" overflow="scroll">
<xsl:variable name="orm-model" select="ormRoot:ORM2/orm:ORMModel"/>
<xsl:apply-templates select="ormRoot:ORM2/ormDiagram:ORMDiagram">
<xsl:with-param name="orm-model" select="$orm-model" tunnel="yes"/>
</xsl:apply-templates>
</svg:svg>
</xsl:result-document>
</xsl:template>
<xsl:template match="ormDiagram:ORMDiagram">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:apply-templates select="ormDiagram:Shapes">
<xsl:with-param name="diagram" select="." tunnel="yes"/>
</xsl:apply-templates>
</xsl:template>
<xsl:template match="ormDiagram:Shapes">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:param name="diagram" tunnel="yes"/>
<xsl:apply-templates select="ormDiagram:FactTypeShape"/>
<xsl:apply-templates select="ormDiagram:ObjectTypeShape"/>
<xsl:apply-templates select="ormDiagram:ExternalConstraintShape"/>
</xsl:template>
<xsl:template mode="bounds-value" match="@AbsoluteBounds">
</xsl:template>
<xsl:template match="ormDiagram:ObjectTypeShape">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:param name="diagram" tunnel="yes"/>

<xsl:variable
name="bounds"
select="orm2d4:bounds-parse(@AbsoluteBounds)"/>
<xsl:variable name="object-type-guid" select="ormDiagram:Subject/@ref"/>
<xsl:variable name="object-type" select="$orm-model/orm:Objects/orm:*[@id = $object-type-guid]"/>
<xsl:variable name="stroke-dasharray" select="if (name($object-type) = 'orm:EntityType') then '0' else '4.8,1.6'"/>
<svg:rect x="{$bounds/@x}in" y="{$bounds/@y}in" width="{$bounds/@width}in" 
height="{$bounds/@height}in"
rx="5.669291338582678"
ry="5.669291338582678"
style="fill:#ffffff;stroke-width:1.6;
stroke:#00008b;
stroke-dasharray: {$stroke-dasharray}" />
<!-- render entity type name -->
<xsl:variable name="name-pt" select="orm2d4:bounds-center($bounds)"/>
<xsl:variable name="font-name" select="$diagram/@BaseFontName"/>
<xsl:variable name="font-size" select="$diagram/@BaseFontSize"/>
<svg:text
style="fill:black; font-style:normal;text-anchor:middle;alignment-baseline:middle;font-weight:bold;font-family: {$font-name};font-size: {$font-size}in"
x="{$name-pt/@x}in" y="{$name-pt/@y}in">
<xsl:value-of select="$object-type/@Name"/>
</svg:text>
<!-- TODO: render refmode for entity types? -->
</xsl:template>

<xsl:variable name="rolebox-width" select="0.16"/>
<xsl:variable name="rolebox-height" select="0.11"/>

<xsl:template name="compute-role-box-layout">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:param name="fact-type-shape"/>

<xsl:variable
name="bounds"
select="orm2d4:bounds-parse($fact-type-shape/@AbsoluteBounds)"/>
<xsl:variable name="fact-type-guid" select="$fact-type-shape/ormDiagram:Subject/@ref"/>
<xsl:variable name="fact-type" select="$orm-model/orm:Facts/orm:Fact[@id = $fact-type-guid]"/>

<xsl:variable
name="constraint-display-position"
select="if ($fact-type-shape/@ConstraintDisplayPosition)
then $fact-type-shape/@ConstraintDisplayPosition
else 'Top'"/>
<!--
unaries are stored as binary fact types,
functional w.r.t. the object type that the unary is attached to
- first role player: object type that the unary was attached to
- second role player: an implicit value type
-->
<xsl:variable name="roles">
<xsl:choose>
<xsl:when
test="count($fact-type/orm:FactRoles/orm:Role) = 2 and
(($orm-model/orm:Objects/orm:ValueType
[@id = $fact-type/orm:FactRoles/orm:Role[2]/orm:RolePlayer/@ref])/@IsImplicitBooleanValue = 'true')">
<xsl:for-each select="$fact-type/orm:FactRoles/orm:Role[1]">
<orm2d4:role id="{@id}"/>
</xsl:for-each>
</xsl:when>
<xsl:when test="$fact-type-shape/ormDiagram:RoleDisplayOrder">
<xsl:for-each select="$fact-type-shape/ormDiagram:RoleDisplayOrder/ormDiagram:Role">
<orm2d4:role id="{@ref}"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<xsl:for-each select="$fact-type/orm:FactRoles/orm:Role">
<orm2d4:role id="{@id}"/>
</xsl:for-each>
</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:choose>
<xsl:when test="$fact-type-shape/@DisplayOrientation = 'Horizontal' or not($fact-type-shape/@DisplayOrientation)">
<xsl:variable
name="offset-x"
select="($bounds/@width - $rolebox-width * count($roles/orm2d4:role)) div 2"/>
<xsl:variable
name="offset-y"
select="if ($constraint-display-position = 'Top') then 0 else $rolebox-height"/>
<xsl:call-template name="calculate-role-shapes-bounds">
<xsl:with-param name="roles" select="$roles/orm2d4:role"/>
<xsl:with-param
name="start"
select="orm2d4:point-make(
$bounds/@x + $offset-x,
$bounds/@y + (if ($constraint-display-position = 'Top')
then $bounds/@height - $rolebox-height - 0.02
else 0.02))"/>
<xsl:with-param name="step" select="orm2d4:vector-make($rolebox-width,0)"/>
<xsl:with-param name="width" select="$rolebox-width"/>
<xsl:with-param name="height" select="$rolebox-height"/>
</xsl:call-template>
</xsl:when>
<xsl:when test="$fact-type-shape/@DisplayOrientation = 'VerticalRotatedRight'">
<xsl:variable
name="offset-x"
select="if ($constraint-display-position = 'Top') then 0 else $rolebox-height"/>
<xsl:variable
name="offset-y"
select="($bounds/@width - $rolebox-width * count($roles/orm2d4:role)) div 2"/>
<xsl:call-template name="calculate-role-shapes-bounds">
<xsl:with-param name="roles" select="$roles/orm2d4:role"/>
<xsl:with-param
name="start"
select="orm2d4:point-make(
$bounds/@x + (if ($constraint-display-position = 'Bottom')
then $bounds/@width - $rolebox-height - 0.02
else 0.02), $bounds/@y)"/>
<xsl:with-param name="step" select="orm2d4:vector-make(0,$rolebox-width)"/>
<xsl:with-param name="width" select="$rolebox-height"/>
<xsl:with-param name="height" select="$rolebox-width"/>
</xsl:call-template>
</xsl:when>
<xsl:when test="$fact-type-shape/@DisplayOrientation = 'VerticalRotatedLeft'">
<xsl:variable
name="offset-x"
select="if ($constraint-display-position = 'Top') then $rolebox-height else 0"/>
<xsl:variable
name="offset-y"
select="($bounds/@width - $rolebox-width * count($roles/orm2d4:role)) div 2"/>
<xsl:call-template name="calculate-role-shapes-bounds">
<xsl:with-param name="roles" select="$roles/orm2d4:role"/>
<xsl:with-param
name="start"
select="orm2d4:point-make(
$bounds/@x + (if ($constraint-display-position = 'Top')
then $bounds/@width - $rolebox-height - 0.02
else 0.02), $bounds/@y + $bounds/@height - $rolebox-width)"/>
<xsl:with-param name="step" select="orm2d4:vector-make(0,-$rolebox-width)"/>
<xsl:with-param name="width" select="$rolebox-height"/>
<xsl:with-param name="height" select="$rolebox-width"/>
<xsl:with-param name="sort-order" select="'descending'"/>
</xsl:call-template>
</xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template name="calculate-role-shapes-bounds">
<xsl:param name="roles"/>
<xsl:param name="start"/>
<xsl:param name="step"/>
<xsl:param name="width"/>
<xsl:param name="height"/>
<xsl:param name="sort-order" select="'ascending'"/>

<xsl:for-each select="$roles">
<xsl:sort select="position()" data-type="number" order="{$sort-order}"/>
<xsl:variable name="pt" select="orm2d4:point-move($start, position() - 1, $step)"/>
<orm2d4:roleinfo>
<orm2d4:role ref="{@id}" />
<xsl:sequence select="orm2d4:bounds-make($pt/@x, $pt/@y, $width, $height)"/>
</orm2d4:roleinfo>
</xsl:for-each>
</xsl:template>

<xsl:template match="ormDiagram:FactTypeShape">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:param name="diagram" tunnel="yes"/>

<xsl:variable
name="bounds"
select="orm2d4:bounds-parse(@AbsoluteBounds)"/>
<xsl:variable name="fact-type-guid" select="ormDiagram:Subject/@ref"/>
<xsl:variable name="fact-type" select="$orm-model/orm:Facts/orm:Fact[@id = $fact-type-guid]"/>
<!-- xsl:if test="count($fact-type/orm:FactRoles/orm:Role) = 2">
<xsl:variable name="ref" select="$fact-type/orm:FactRoles/orm:Role[2]/orm:RolePlayer/@ref"/>
<pre>ref: <xsl:value-of select="$ref"/></pre>
<pre>unary: <xsl:copy-of
select="$orm-model/orm:Objects/orm:ValueType[@id = $ref][@IsImplicitBooleanValue = 'true']"/></pre></xsl:if>-->
<!--
NOTE: unfortunately for us, SVG transform does not allow for units to be specified
(which is what we need to let the SVG processor do inch->pixel conversions for us),
therefore the plan to use relative positioning will not work, hence we specify
absolute coordinates everywhere
 -->
<xsl:if test="$debug-rects">
<svg:rect
x="{$bounds/@x}in" y="{$bounds/@y}in"
width="{$bounds/@width}in" height="{$bounds/@height}in"
fill="white" stroke="black"/>
</xsl:if>
<xsl:variable name="role-shapes-bounds">
<xsl:call-template name="compute-role-box-layout">
<xsl:with-param name="fact-type-shape" select="."/>
</xsl:call-template>
</xsl:variable>
<xsl:variable
name="constraint-display-position"
select="if (@ConstraintDisplayPosition) then @ConstraintDisplayPosition else 'Top'"/>
<xsl:choose>
<xsl:when test="@DisplayOrientation = 'Horizontal' or not(@DisplayOrientation)">
<xsl:variable
name="dir"
select="if ($constraint-display-position = 'Top')
then orm2d4:vector-make(0,-1)
else orm2d4:vector-make(0,1)"/>
<xsl:variable
name="side-idx"
select="if ($constraint-display-position = 'Top') then 1 else 3"/>
<xsl:call-template name="render-fact-type">
<xsl:with-param name="fact-type" select="$fact-type" tunnel="yes"/>
<xsl:with-param name="bounds" select="$bounds" tunnel="yes"/>
<xsl:with-param name="role-shapes-bounds" select="$role-shapes-bounds"/>
<xsl:with-param name="side-idx" select="$side-idx"/>
<xsl:with-param name="dir" select="$dir"/>
</xsl:call-template>
</xsl:when>
<xsl:when test="@DisplayOrientation = 'VerticalRotatedRight'">
<xsl:variable
name="dir"
select="if ($constraint-display-position = 'Top')
then orm2d4:vector-make(1,0)
else orm2d4:vector-make(-1,0)"/>
<xsl:variable
name="side-idx"
select="if ($constraint-display-position = 'Top') then 2 else 4"/>
<xsl:call-template name="render-fact-type">
<xsl:with-param name="fact-type" select="$fact-type" tunnel="yes"/>
<xsl:with-param name="bounds" select="$bounds" tunnel="yes"/>
<xsl:with-param name="role-shapes-bounds" select="$role-shapes-bounds"/>
<xsl:with-param name="side-idx" select="$side-idx"/>
<xsl:with-param name="dir" select="$dir"/>
</xsl:call-template>
</xsl:when>
<xsl:when test="@DisplayOrientation = 'VerticalRotatedLeft'">
<xsl:variable
name="dir"
select="if ($constraint-display-position = 'Top')
then orm2d4:vector-make(-1,0)
else orm2d4:vector-make(1,0)"/>
<xsl:variable
name="side-idx"
select="if ($constraint-display-position = 'Top') then 4 else 2"/>
<xsl:call-template name="render-fact-type">
<xsl:with-param name="fact-type" select="$fact-type" tunnel="yes"/>
<xsl:with-param name="bounds" select="$bounds" tunnel="yes"/>
<xsl:with-param name="role-shapes-bounds" select="$role-shapes-bounds"/>
<xsl:with-param name="side-idx" select="$side-idx"/>
<xsl:with-param name="dir" select="$dir"/>
</xsl:call-template>
</xsl:when>
</xsl:choose>
<xsl:apply-templates select="ormDiagram:RelativeShapes/ormDiagram:*">
<xsl:with-param name="fact-type" select="$fact-type" tunnel="yes"/>
</xsl:apply-templates>
</xsl:template>

<xsl:template name="render-fact-type">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:param name="diagram" tunnel="yes"/>
<xsl:param name="bounds" tunnel="yes"/>
<xsl:param name="fact-type" tunnel="yes"/>
<xsl:param name="role-shapes-bounds"/>
<xsl:param name="side-idx"/>
<xsl:param name="dir"/>

<!-- <pre><xsl:copy-of select="$role-shapes-bounds"/></pre> -->
<xsl:for-each select="$role-shapes-bounds/orm2d4:roleinfo">
<xsl:variable name="role-guid" select="orm2d4:role/@ref"/>
<xsl:call-template name="render-role">
<xsl:with-param name="bounds" select="$bounds"/>
<xsl:with-param name="role-shape-bounds" select="orm2d4:bounds"/>
<xsl:with-param name="role" select="$fact-type/orm:FactRoles/orm:Role[@id = $role-guid]"/>
</xsl:call-template>
</xsl:for-each>
<xsl:variable name="offset-nudge" select="0.04"/>
<!-- <pre>
dir: <xsl:copy-of select="$dir"/>
side-idx: <xsl:copy-of select="$side-idx"/>
</pre>-->
<xsl:call-template name="render-internal-uniqueness-constraint">
<xsl:with-param name="bounds" select="$bounds" tunnel="yes"/>
<xsl:with-param name="role-shapes-bounds" select="$role-shapes-bounds" tunnel="yes"/>
<xsl:with-param name="fact-type" select="$fact-type"/>
<xsl:with-param name="iuc" select="$fact-type/orm:InternalConstraints/orm:UniquenessConstraint[1]"/>
<xsl:with-param name="iuc-rest" select="$fact-type/orm:InternalConstraints/orm:UniquenessConstraint[position() &gt; 1]"/>
<xsl:with-param name="offset" select="orm2d4:point-move(orm2d4:point-make(0,0), $offset-nudge, $dir)"/>
<xsl:with-param name="dir" select="$dir"/>
<xsl:with-param name="side-idx" select="$side-idx"/>
<xsl:with-param name="offset-nudge" select="$offset-nudge"/>
</xsl:call-template>
</xsl:template>
<xsl:template name="render-internal-uniqueness-constraint">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:param name="diagram" tunnel="yes"/>
<xsl:param name="bounds" tunnel="yes"/>
<xsl:param name="role-shapes-bounds" tunnel="yes"/>
<xsl:param name="fact-type" tunnel="yes"/>
<xsl:param name="iuc"/>
<xsl:param name="iuc-rest"/>
<xsl:param name="side-idx"/>
<xsl:param name="dir"/>
<xsl:param name="offset"/>
<xsl:param name="offset-nudge"/>

<xsl:variable name="uc-guid" select="$iuc/@ref"/>
<xsl:variable name="uc" select="$orm-model/orm:Constraints/orm:UniquenessConstraint[@id = $uc-guid]"/>
<xsl:variable name="offset-x" select="0.02"/>
<!-- <pre><xsl:copy-of select="$uc"/></pre>-->
<xsl:variable name="uc-arity" select="count($uc/orm:RoleSequence/orm:Role)"/>

<!--<pre>offset: <xsl:copy-of select="$offset"/></pre>-->

<!-- draw UC bars over role boxes -->
<xsl:for-each select="$role-shapes-bounds/orm2d4:roleinfo">
<xsl:variable name="role-guid" select="orm2d4:role/@ref"/>
<xsl:variable
name="sides"
select="orm2d4:points-to-linesegs(orm2d4:bounds-to-points(orm2d4:bounds))"/>
<!-- <pre>sides: <xsl:copy-of select="$sides"/></pre> -->
<xsl:variable name="side" select="$sides/orm2d4:lineseg[position() = $side-idx]"/>
<!-- <pre>side: <xsl:copy-of select="$side"/></pre> -->
<xsl:variable name="side-midpoint" select="orm2d4:lineseg-center($side)"/>
<xsl:variable name="side-direction" select="orm2d4:vector-make($side/@x2 - $side/@x1, $side/@y2 - $side/@y1)"/>
<xsl:variable name="side-length">
<xsl:variable name="math-obj" select="ixsl:get(ixsl:window(), 'Math')"/>
<xsl:sequence
select="ixsl:call($math-obj, 'sqrt',
$side-direction/@x * $side-direction/@x  + $side-direction/@y * $side-direction/@y)"/>
</xsl:variable>
<xsl:variable
name="side-direction-unit"
select="orm2d4:vector-make($side-direction/@x div $side-length, $side-direction/@y div $side-length)"/>
<xsl:variable name="point-a" select="orm2d4:point-move($side-midpoint, $side-length * 0.4, $side-direction-unit)"/>
<xsl:variable name="point-b" select="orm2d4:point-move($side-midpoint, -$side-length * 0.4, $side-direction-unit)"/>
<xsl:variable name="bar-lineseg">
<orm2d4:lineseg
x1="{$point-a/@x + $offset/@x}"
y1="{$point-a/@y + $offset/@y}"
x2="{$point-b/@x + $offset/@x}"
y2="{$point-b/@y + $offset/@y}"/>
</xsl:variable>
<!-- <pre>bar-lineseg: <xsl:copy-of select="$bar-lineseg"/></pre> -->
<xsl:choose>
<!-- if role occurs in this IUC -->
<xsl:when test="$uc/orm:RoleSequence/orm:Role[@ref = $role-guid]">
<svg:line
x1="{$bar-lineseg/orm2d4:lineseg/@x1}in"
y1="{$bar-lineseg/orm2d4:lineseg/@y1}in"
x2="{$bar-lineseg/orm2d4:lineseg/@x2}in"
y2="{$bar-lineseg/orm2d4:lineseg/@y2}in"
style="stroke:#800080;stroke-linecap: round;stroke-width:1.6"/>
</xsl:when>
<xsl:otherwise>
<xsl:variable name="following-role" select="following-sibling::orm2d4:roleinfo[1]/orm2d4:role"/>
<!-- <pre>
current role not in this IUC
following role: <xsl:copy-of select="$following-role"/>
following role guid: <xsl:value-of select="$following-role/@ref"/>
following role in this IUC: <xsl:copy-of select="$uc/orm:RoleSequence/orm:Role[@ref = $following-role/@ref]"/>
</pre>-->
<xsl:if test="$uc-arity &gt;= 2 and count($uc/orm:RoleSequence/orm:Role[@ref = $following-role/@ref]) &gt; 0">
<svg:line
x1="{$bar-lineseg/orm2d4:lineseg/@x1}in"
y1="{$bar-lineseg/orm2d4:lineseg/@y1}in"
x2="{$bar-lineseg/orm2d4:lineseg/@x2}in"
y2="{$bar-lineseg/orm2d4:lineseg/@y2}in"
style="stroke:#800080;stroke-linecap: round;stroke-width:1.6"
stroke-dasharray="1.6,2"
/>
</xsl:if>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

<!-- see if there is more to do -->
<xsl:variable name="next-iuc" select="$iuc-rest[1]"/>
<xsl:if test="$next-iuc">
<!-- <pre>more: <xsl:copy-of select="$next-iuc"/></pre> -->
<xsl:variable name="next-iuc-guid" select="$next-iuc/@ref"/>
<xsl:variable name="next-uc" select="$orm-model/orm:Constraints/orm:UniquenessConstraint[@id = $next-iuc-guid]"/>
<xsl:variable name="iuc-rest" select="$iuc-rest[position() &gt; 1]"/>
<!-- <pre>A: <xsl:copy-of select="$uc/orm:RoleSequence/orm:Role"/>
B: <xsl:copy-of select="$next-uc/orm:RoleSequence/orm:Role"/>
intersection result:
<xsl:copy-of select="$uc/orm:RoleSequence/orm:Role[@ref = $next-uc/orm:RoleSequence/orm:Role/@ref]"/>
</pre>-->
<xsl:variable name="overlap" select="$uc/orm:RoleSequence/orm:Role[@ref = $next-uc/orm:RoleSequence/orm:Role/@ref]"/>
<xsl:call-template name="render-internal-uniqueness-constraint">
<xsl:with-param name="fact-type" select="$fact-type"/>
<xsl:with-param name="iuc" select="$next-iuc"/>
<xsl:with-param name="iuc-rest" select="$iuc-rest"/>
<xsl:with-param name="offset-nudge" select="$offset-nudge"/>
<xsl:with-param name="side-idx" select="$side-idx"/>
<xsl:with-param name="dir" select="$dir"/>
<xsl:with-param
name="offset"
select="if ($overlap) then (orm2d4:point-move($offset, $offset-nudge, $dir)) else $offset"/>
</xsl:call-template>
</xsl:if>
</xsl:template>

<xsl:template name="render-role">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:param name="diagram" tunnel="yes"/>
<xsl:param name="bounds"/>
<xsl:param name="role-shape-bounds"/>
<xsl:param name="role"/>

<xsl:variable name="role-player-guid" select="$role/orm:RolePlayer/@ref"/>
<!-- <pre>role: <xsl:copy-of select="$role"/></pre> -->
<!--
TODO: pick the nearest shape, since in general there may be several
shapes for one object type on the same diagram

only works in Chrome:
[min(orm2d4:point-distance-squared(
orm2d4:bounds-center(orm2d4:bounds-parse(@AbsoluteBounds)),
orm2d4:bounds-center($role-shape-bounds)))]
-->
<xsl:variable
name="role-player"
select="($diagram/ormDiagram:Shapes/ormDiagram:ObjectTypeShape
[ormDiagram:Subject/@ref = $role-player-guid])
"/>
<xsl:variable
name="role-player-shape-center"
select="orm2d4:bounds-center(orm2d4:bounds-parse($role-player/@AbsoluteBounds))"/>
<svg:rect
x="{$role-shape-bounds/@x}in"
y="{$role-shape-bounds/@y}in"
width="{$role-shape-bounds/@width}in"
height="{$role-shape-bounds/@height}in"
stroke="black"
fill="white"/>
<xsl:variable name="edge-index" select="orm2d4:edge-index-for-point($role-shape-bounds, $role-player-shape-center)"/>
<xsl:variable name="role-shape-points" select="orm2d4:bounds-to-points($role-shape-bounds)"/>
<xsl:variable name="role-shape-edges" select="orm2d4:points-to-linesegs($role-shape-points)"/>
<!--
<pre><xsl:copy-of select="$role-shape-points"/></pre>
<pre><xsl:copy-of select="$role-shape-edges"/></pre>
<pre><xsl:value-of select="$edge-index"/></pre>-->
<xsl:variable name="edge" select="$role-shape-edges/orm2d4:lineseg[position() = $edge-index]"/>
<!-- <pre><xsl:copy-of select="$edge"/></pre> -->
<xsl:variable name="edge-midpoint" select="orm2d4:lineseg-center($edge)"/>
<svg:line x1="{$edge-midpoint/@x}in" y1="{$edge-midpoint/@y}in"
x2="{$role-player-shape-center/@x}in" y2="{$role-player-shape-center/@y}in"
style="stroke:black;stroke-width:1.6"/>
<xsl:if test="@_IsMandatory='true'">
<!--
we assume that if this attribute is set, then there is a simple non-implied mandatory
constraint that this role is constrained by
 -->
<svg:circle
cx="{$edge-midpoint/@x}in" cy="{$edge-midpoint/@y}in"
r="3.6" fill="#800080" stroke="#800080" stroke-width="1.3"/>
</xsl:if>
</xsl:template>
<xsl:template match="ormDiagram:ReadingShape">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:param name="diagram" tunnel="yes"/>
<xsl:param name="fact-type" tunnel="yes"/>

<xsl:variable
name="arity"
select="count($fact-type/orm:FactRoles/orm:Role)"/>
<xsl:variable
name="bounds"
select="orm2d4:bounds-parse(@AbsoluteBounds)"/>
<xsl:variable name="reading-order-guid" select="ormDiagram:Subject/@ref"/>

<xsl:variable name="reading-order" select="$fact-type/orm:ReadingOrders/orm:ReadingOrder[@id = $reading-order-guid]"/>
<xsl:variable name="font-name" select="$diagram/@BaseFontName"/>
<xsl:variable name="font-size" select="$diagram/@BaseFontSize"/>
<xsl:if test="$debug-rects">
<svg:rect
x="{$bounds/@x}in" y="{$bounds/@y}in"
width="{$bounds/@width}in" height="{$bounds/@height}in"
fill="white" stroke="black"/>
</xsl:if>
<!-- <p>arity is: <xsl:copy-of select="$arity"/></p>-->
<svg:text
style="fill:black;font-style:normal;font-weight:bold;font-family: {$font-name};font-size: {$font-size}in"
x="{$bounds/@x}in"
y="{$bounds/@y + $bounds/@height}in">
<xsl:choose>
<xsl:when test="$arity &lt;= 2">
<xsl:for-each select="$reading-order/orm:Readings/orm:Reading">
<xsl:if test="position()>1">
<xsl:text> / </xsl:text>
</xsl:if>
<xsl:value-of select="replace(orm:Data/text(), '\{\d+\}', '')"/>
</xsl:for-each>
</xsl:when>
<xsl:otherwise>
<!-- arity > 2 -->
<xsl:value-of select="replace($reading-order/orm:Readings/orm:Reading[1]/orm:Data/text(), '\{\d+\}', '…')"/>
</xsl:otherwise>
</xsl:choose>
</svg:text>
</xsl:template>

<!-- TODO: draw model notes too -->

<xsl:template match="ormDiagram:ExternalConstraintShape">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:param name="diagram" tunnel="yes"/>

<xsl:variable
name="bounds"
select="orm2d4:bounds-parse(@AbsoluteBounds)"/>
<xsl:variable name="external-constraint-guid" select="ormDiagram:Subject/@ref"/>
<xsl:variable name="external-constraint" select="$orm-model/orm:Constraints/orm:*[@id = $external-constraint-guid]"/>
<pre><xsl:copy-of select="$external-constraint"/></pre>
<xsl:if test="$debug-rects">
<svg:rect
x="{$bounds/@x}in" y="{$bounds/@y}in"
width="{$bounds/@width}in" height="{$bounds/@height}in"
fill="white" stroke="black"/>
</xsl:if>
<xsl:variable name="center" select="orm2d4:bounds-center($bounds)"/>
<xsl:variable name="radius" select="$bounds/@width div 2"/>
<xsl:choose>
<!--
FIXME: explicit case analysis is unidiomatic, but doing an apply-templates
with select="$external-constraint" and its variations aimed at making
the processor do it for us does not work
-->
<!-- NOTE: graphics translated from NORMA source code -->
<xsl:when test="local-name($external-constraint) = 'UniquenessConstraint'">
<xsl:apply-templates select="$external-constraint/orm:RoleSequence">
<xsl:with-param name="center" select="$center"/>
</xsl:apply-templates>
<svg:circle
cx="{$center/@x}in"
cy="{$center/@y}in"
r="{$radius}in"
stroke="#800080"
stroke-width="1.3" fill="white"/>
<xsl:variable name="y" select="$bounds/@y + $bounds/@height div 2"/>
<xsl:choose>
<xsl:when test="$external-constraint/orm:PreferredIdentifierFor">
<xsl:variable name="y-offset" select="0.02 * 0.7"/>
<svg:line
x1="{$bounds/@x}in"
y1="{$y - $y-offset}in"
x2="{$bounds/@x + $bounds/@width}in"
y2="{$y - $y-offset}in"
stroke="#800080" stroke-width="1.3"/>
<svg:line
x1="{$bounds/@x}in"
y1="{$y + $y-offset}in"
x2="{$bounds/@x + $bounds/@width}in"
y2="{$y + $y-offset}in"
stroke="#800080" stroke-width="1.3"/>
</xsl:when>
<xsl:otherwise>
<svg:line
x1="{$bounds/@x}in"
y1="{$y}in"
x2="{$bounds/@x + $bounds/@width}in"
y2="{$y}in"
stroke="#800080" stroke-width="1.3"/>
</xsl:otherwise>
</xsl:choose>
</xsl:when>
<xsl:when test="local-name($external-constraint) = 'ExclusionConstraint'">
<xsl:apply-templates select="$external-constraint/orm:RoleSequences/orm:RoleSequence">
<xsl:with-param name="center" select="$center"/>
</xsl:apply-templates>
<svg:circle
cx="{$center/@x}in"
cy="{$center/@y}in"
r="{$radius}in"
stroke="#800080"
stroke-width="1.3" fill="white"/>
<xsl:variable name="cos45" select="0.70710678118654752440084436210485"/>
<xsl:variable name="offset" select="$bounds/@width * (1 - $cos45) div 2"/>
<xsl:variable name="left-x" select="$bounds/@x + $offset"/>
<xsl:variable name="right-x" select="$bounds/@x + $bounds/@width - $offset"/>
<xsl:variable name="top-y" select="$bounds/@y + $offset"/>
<xsl:variable name="bottom-y" select="$bounds/@y + $bounds/@height - $offset"/>
<svg:line
x1="{$left-x}in" y1="{$top-y}in" x2="{$right-x}in" y2="{$bottom-y}in"
stroke="#800080"
stroke-width="1.3"/>
<svg:line
x1="{$left-x}in" y1="{$bottom-y}in" x2="{$right-x}in" y2="{$top-y}in"
stroke="#800080"
stroke-width="1.3"/>
</xsl:when>
<xsl:when test="local-name($external-constraint) = 'EqualityConstraint'">
<xsl:apply-templates select="$external-constraint/orm:RoleSequences/orm:RoleSequence">
<xsl:with-param name="center" select="$center"/>
</xsl:apply-templates>
<svg:circle
cx="{$center/@x}in"
cy="{$center/@y}in"
r="{$radius}in"
stroke="#800080"
stroke-width="1.3" fill="white"/>
<xsl:variable name="offset" select="$bounds/@width * 0.3"/>
<xsl:variable name="left-x" select="$bounds/@x + $offset"/>
<xsl:variable name="right-x" select="$bounds/@x + $bounds/@width - $offset"/>
<xsl:variable name="y" select="$center/@y"/>
<xsl:variable name="y-offset" select="0.01"/>

<svg:line
x1="{$left-x}in" y1="{$y - $y-offset}in" x2="{$right-x}in" y2="{$y - $y-offset}in"
stroke="#800080"
stroke-width="1.3"/>
<svg:line
x1="{$left-x}in" y1="{$y + $y-offset}in" x2="{$right-x}in" y2="{$y + $y-offset}in"
stroke="#800080"
stroke-width="1.3"/>
</xsl:when>
<xsl:when test="local-name($external-constraint) = ''">

</xsl:when>
</xsl:choose>
<!-- TODO:
- draw the constraint shape
- FIXME: where do I get the images?
    - looks like I'll have to draw something myself...
        + EUC: circle with horizontal line across
        - equality C: circle with two lines resembling =
        + exclusion C: circle with two lines, one at 45 degrees, another at -45, crossing at the center
        - inclusive or C: circle with a dot (dot is the same as on the mandatory constraint)
        - exclusive or C: inclusive or C and exclusion C combined
        - subset C: circle with \subseteq sign
        - frequency C: circle with \geq sign
        - ring constraint: many possibilities
        - FIXME: will also have to add deontic constraints at one point
    - on referencing the same image multiple times: http://docs.webplatform.org/wiki/svg/tutorials/smarter_svg_graphics
 -->
</xsl:template>
<xsl:template match="orm:RoleSequence">
<xsl:param name="orm-model" tunnel="yes"/>
<xsl:param name="diagram" tunnel="yes"/>
<xsl:param name="center"/>

<xsl:for-each select="orm:Role">
<!-- given @ref, determine role box shape for the fact type shape the role is in -->
<xsl:variable name="role-guid" select="@ref"/>
<!-- <pre>role guid: <xsl:value-of select="$role-guid"/></pre> -->
<xsl:variable name="fact-type" select="$orm-model/orm:Facts/orm:Fact[orm:FactRoles/orm:Role/@id = $role-guid]"/>
<!-- <pre>fact type: <xsl:copy-of select="$fact-type"/></pre> -->
<xsl:variable name="fact-type-guid" select="$fact-type/@id"/>
<!-- FIXME: won't work if there are multiple shapes for one fact type -->
<xsl:variable name="fact-type-shape" select="$diagram/ormDiagram:Shapes/ormDiagram:FactTypeShape[ormDiagram:Subject/@ref = $fact-type-guid]"/>
<!-- <pre>fact type shape: <xsl:copy-of select="$fact-type-shape"/></pre> -->
<xsl:variable name="role-boxes">
<xsl:call-template name="compute-role-box-layout">
<xsl:with-param name="orm-model" select="$orm-model"/>
<xsl:with-param name="fact-type-shape" select="$fact-type-shape"/>
</xsl:call-template>
</xsl:variable>
<!-- <pre>role boxes: <xsl:copy-of select="$role-boxes"/></pre> -->

<xsl:variable name="roleinfo" select="$role-boxes/orm2d4:roleinfo[orm2d4:role/@ref = $role-guid]"/>
<xsl:variable name="edge-index" select="orm2d4:edge-index-for-point($roleinfo/orm2d4:bounds, $center)"/>
<xsl:variable name="rolebox-points" select="orm2d4:bounds-to-points($roleinfo/orm2d4:bounds)"/>
<xsl:variable name="rolebox-edges" select="orm2d4:points-to-linesegs($rolebox-points)"/>
<xsl:variable name="edge" select="$rolebox-edges/orm2d4:lineseg[position() = $edge-index]"/>
<xsl:variable name="edge-midpoint" select="orm2d4:lineseg-center($edge)"/>

<svg:line
x1="{$edge-midpoint/@x}in"
y1="{$edge-midpoint/@y}in"
x2="{$center/@x}in"
y2="{$center/@y}in"
stroke="#800080"
stroke-width="1.3"
stroke-dasharray="4,1.3"/>
</xsl:for-each>
</xsl:template>
</xsl:transform>
